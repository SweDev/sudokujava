package Sudoku_gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import Sudoku_model.Sudoku;
/**
 * This class handles the GUI of the 
 * Sudoku program.
 * 
 * @author      Patrik Paradis
 * @version		LTH University, ready for assessment version.
 */
public class SudokuGUI  extends JFrame implements ActionListener{
	private Sudoku game;	

	private JFrame frame;
	private JPanel northPanel;
	private JPanel southPanel;
	private JButton solveButton;
	private JButton clearButton;
	
	// Variables used in many loops throughout the class.
	// Let's us know how many rows and columns a Sudoku game board contains.
	private final static int ROWS = 9;
	private final static int COLS = 9;
	// This is the actual game board on the GUI side represented by text fields objects.
	// Will reference 81 text field objects.
	private static JTextField[][] board = new JTextField[ROWS][COLS];
	
	/*
	 * Constructor that creates a GUI that a human
	 * sees when she/he plays the game. The constructor
	 * takes a reference to the sudoku game model as well,
	 * and is used when creating the GUI for the game
	 * board.
	 */
	public SudokuGUI(Sudoku game){
		this.game = game;
		
		frame = new JFrame("Sudoku Solver");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(9, 9));
		southPanel = new JPanel();
		
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				board[row][col] = new JTextField(2);
				northPanel.add(board[row][col]);
				// Color some of the boxes of the game board.
				if(1 == game.getBoxId(row, col) ||  // Box 1
						3 == game.getBoxId(row, col) ||  // Box 3
						5 == game.getBoxId(row, col) ||  // Box 5
						7 == game.getBoxId(row, col) ||  // Box 7
						9 == game.getBoxId(row, col))	// Box 9
					board[row][col].setBackground(Color.gray);
				board[row][col].setBorder(new LineBorder(Color.black, 1));
				board[row][col].setEditable(true);
				board[row][col].setFont(new Font(null,Font.BOLD,16));
				col++;
			}
			row++;
		}
		
		frame.add(northPanel,BorderLayout.NORTH);
		frame.add(southPanel,BorderLayout.SOUTH);
		
		solveButton = new JButton("Solve");
		clearButton = new JButton("Clear");
		solveButton.addActionListener(this);
		clearButton.addActionListener(this);
		
		southPanel.add(solveButton, BorderLayout.WEST);
		southPanel.add(clearButton, BorderLayout.EAST);
		
		frame.pack();
		frame.setVisible(true);
	}
	
	/*
	 * Removes all the numbers present on the
	 * game board.
	 */
	private void clearBoard(){
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				board[row][col].setText("");
				col++;
			}
			row++;
		}
	}
	
	/*
	 * Solves the Sudoku and present the solution in the
	 * GUI for the human player.
	 */
	private boolean solveSudoku(){
		// Read values from GUI to the model.
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				if(0 != board[row][col].getText().length()){
					game.setNumber(Integer.valueOf(board[row][col].getText()),row, col);
					game.setHasNumber(true, row, col);
				}
				col++;
			}
			row++;
		}

		// Read values from model to GUI.
		if(game.solve()){
			row = 0;
			while(row < ROWS){
				int col = 0;
				while(col < COLS){
					if(game.getHasNumber(row, col)){
						board[row][col].setText(String.valueOf(game.getNumber(row, col)));
					}
					col++;
				}
				row++;
			}			
			return true;
		}
		else
			return false;
	}
	
	/*
	 * Checks the board if the values 
	 * that each square has are correct 
	 * and valid ones. 
	 */
	private boolean validValues(){
		boolean returnValue = false;
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				char[] input =  board[row][col].getText().trim().toCharArray();
				if(1 == input.length){
					if(Character.isDigit(input[0]) && ('0' != input[0]) && !Character.isAlphabetic(input[0]))
						returnValue = true;
					else{
						return false;
					}
				}
				else if(0 == input.length){
					returnValue = true;
				}
				else if(1 < input.length){
					return false;
				}
				col++;
			}
			row++;
		}
		return returnValue;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean valid = false;
		if (e.getSource() == solveButton){
			valid = validValues();
			if(valid != true)
				JOptionPane.showMessageDialog(this,"Please use valid inputs and try again");
			else{
				if(solveSudoku())
					JOptionPane.showMessageDialog(this,"Solution was found!");
				else
					JOptionPane.showMessageDialog(this,"Solution not found for these set of values");
			}
		}
		if (e.getSource() == clearButton){
			clearBoard();
		}
	}
	
}
