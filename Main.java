package Sudoku_gui;

import Sudoku_model.Sudoku;
/**
 * Main class of the Sudoku program.
 * This is where the program starts 
 * it's execution.
 * 
 * @author      Patrik Paradis
 * @version		LTH University, ready for assessment version.
 */
public class Main {
	public static void main(String[] args){
		final int ROWS = 9;
		final int COLS = 9;
		
		// Create the game board.
		Sudoku board = new Sudoku();
		// Create and start the GUI board. Will be visible to the human player.
		SudokuGUI gui = new SudokuGUI(board);
	}
}
