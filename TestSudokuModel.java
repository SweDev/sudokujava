package Sudoku_test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Sudoku_model.Sudoku;
/**
 * Test class for testing the Sudoku model.
 * 
 * 
 * @author      Patrik Paradis
 * @version		LTH University, ready for assessment version.
 */
public class TestSudokuModel {
	private Sudoku sudoku;
	
	@Before
	public void setUp() throws Exception {
		sudoku = new Sudoku();
	}

	@After
	public void tearDown() throws Exception {
		sudoku = null;
	}

	@Test
	public void testSudokuRules() {
		sudoku.setNumber(1, 0, 0);
		
		// Test same row sudoku rule.
		assertFalse("Test for same row sudoku rule failed.",sudoku.sudokuRulesCheck(1, 0, 8));
		
		// Test same column sudoku rule.
		assertFalse("Test for same column sudoku rule failed.",sudoku.sudokuRulesCheck(1, 8, 0));
		
		// Test same box sudoku rule.
		assertFalse("Test for same box sudoku rule.",sudoku.sudokuRulesCheck(1, 2, 2));
	}
}
