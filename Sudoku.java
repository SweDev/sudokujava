package Sudoku_model;
/**
 * This Sudoku class represents the board of the 
 * Sudoku game. The board has squares where all
 * the values will be placed either by a human
 * or the computer.
 * 
 * This class creates a board with 81 squares and
 * initializes each square with a proper initial value.
 * The class provides functionality to deliver a full
 * solution to a Sudoku game using recursive backtracking
 * problem solving. 
 * The class also provides a way to verify that the 
 * Sudoku game rules are followed and is used by
 * the recursive backtracking method contained in this
 * class.
 *  
 * This class does not proved any GUI. This class is the 
 * model behind the GUI.
 * 
 * @author      Patrik Paradis
 * @version		LTH University, ready for assessment version.
 */
public class Sudoku {
	
	// Variables used in many loops throughout the class.
	// Let's us know how many rows and columns a Sudoku game board contains.
	private final static int ROWS = 9;
	private final static int COLS = 9;
	// This is the actual game board represented by square objects.
	// Will reference 81 square objects.
	private static Square[][] sudoku = new Square[ROWS][COLS];
	
	/**
	 * Constructor to create a Sudoku game board. 
	 * Creates 81 squares and maps each square with 
	 * an id number representing a box on the game
	 * board.
	 */
	public Sudoku() {
		// Create the sudoku table
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				sudoku[row][col] = new Square();
				
				// Map each square to a box in the table
				if(row<=2 && col<=2)
					sudoku[row][col].setBoxId(1);   // Box 1
				else if(row<=2 && col>2 && col<=5)
					sudoku[row][col].setBoxId(2);	// Box 2
				else if(row<=2 && col>5 && col<=8)
					sudoku[row][col].setBoxId(3);	// Box 3
				else if(row>2 && row<=5 && col<=2)
					sudoku[row][col].setBoxId(4);	// Box 4
				else if(row>2 && row<=5 && col>2 && col<=5)
					sudoku[row][col].setBoxId(5);	// Box 5
				else if(row>2 && row<=5 && col>5 && col<=8)
					sudoku[row][col].setBoxId(6);	// Box 6
				else if(row>5 && row<=8 && col<=2)
					sudoku[row][col].setBoxId(7);	// Box 7
				else if(row>5 && row<=8 && col>2 && col<=5)
					sudoku[row][col].setBoxId(8);	// Box 8
				else if(row>5 && row<=8 && col>5 && col<=8)
					sudoku[row][col].setBoxId(9);	// Box 9
				col++;
			}
			row++;
		}
	}
	
	/**
	 * Exposes a way to find out if a square on the
	 * board has a number or not.
	 * <p>
	 * 
	 * @param row a row on the game board.
	 * @param col a column on the game board.
	 * @return <code>true</code> if the has a number.
	 * 		   <code>false</code> otherwise.
	 */
	public boolean getHasNumber(int row, int col) {
		return sudoku[row][col].getHasNumber();
	}
	
	/**
	 * Exposes a way to set a boolean value (true/false) 
	 * property for a square.
	 * <p>
	 * 
	 * @param hasNumber a boolean value.
	 * @param row a row on the game board.
	 * @param col a column on the game board.
	 */
	public void setHasNumber(boolean hasNumber, int row, int col) {
		sudoku[row][col].setHasNumber(hasNumber);
	}
	
	/**
	 * Exposes a way to set an integer value on a square.
	 * Must be an integer value 1...9.
	 * <p>
	 * 
	 * @param number the number to be assigned onto a square.
	 * @param row a row on the game board.
	 * @param col a column on the game board.
	 */
	public void setNumber(int number, int row, int col) {
		sudoku[row][col].setNumber(number);
	}
	
	/**
	 * Exposes a way to get an integer value that a square has.
	 * <p>
	 * 
	 * @param row a row on the game board.
	 * @param col a column on the game board.
	 * @return an integer number between 1...9.
	 */
	public int getNumber(int row, int col) {
		return sudoku[row][col].getNumber();
	}

	/**
	 * Exposes a way to get an id number that is used to 
	 * map every square on the board to one of nine 
	 * "boxes" on the board.
	 * <p>
	 * 
	 * @param row a row on the game board.
	 * @param col a column on the game board.
	 * @return an integer number between 1...9.
	 */
	public int getBoxId(int row, int col) {
		return sudoku[row][col].getBoxId();
	}
	
	/**
	 * Recursive method that uses the backtracking
	 * algorithm to find the solution for a Sudoku
	 * game.It starts its operations at the first
	 * square of the board (top-leftmost square).
	 * <p>
	 * 
	 * @return <code>true</code> if a solution was found.
	 * 		   <code>false</code> otherwise.
	 */
	public boolean solve(){
		return solve(0,0);
	}
	
	private boolean solve(int i, int j){
		if(!sudoku[i][j].getHasNumber()){

			/* Basecase check */
			if(allSquaresFilledCheck())
				return true;

			/* Recursice case */
			for(int val = 1;val<=9;val++){
				if(sudokuRulesCheck(val, i, j)){
					sudoku[i][j].setNumber(val);
					sudoku[i][j].setHasNumber(true);
					if(solve(i,j))
						return true;
					sudoku[i][j].setHasNumber(false);
					sudoku[i][j].setNumber(-1);
				}
			}
			return false;
		}else {
			/* Basecase check */
			if(allSquaresFilledCheck())
				return true;

			/* Recursive case */
			if(sudokuRulesCheck(sudoku[i][j].getNumber(), i, j)){
				j++;
				if(j == COLS){
					j = 0;
					i++;
				}
				return solve(i,j);
			}
			return false;
		}
	}
	
	/* 
	 * Used as a base case in backtracking algorithm 
	 * and checks if all squares on the game board have
	 * numbers.
	 */
	private boolean allSquaresFilledCheck(){
		boolean hasNumber = false;
		int row = 0;
		while(row < ROWS){
			int col = 0;
			while(col < COLS){
				if(sudoku[row][col].getHasNumber())
					hasNumber = true;
				else
					hasNumber = false;
				col++;
			}
			row++;
		}
		return hasNumber;
	}
	
	/*
	 * Used to verify that a number on the board follows the rules
	 * for Sudoku.
	 */
	public boolean sudokuRulesCheck(int value, int row, int col){

		// Check all values for this row.
		int j = 0;
		while(j < COLS){
			// Exclude column col from check.
			if((j != col) && (sudoku[row][j].getNumber() == value))
				return false;	
			j++;
		}

		// Check all values for this column in the table.
		int i = 0;
		while(i < ROWS){
			// Exclude row, row from check.
			if((i != row) && (sudoku[i][col].getNumber() == value))
				return false;	
			i++;
		}

		// Check all values for this box in the table.
		i = 0;
		while(i < ROWS){
			j = 0;
			while(j < COLS){
				// Exclude col and row from check.
				if((i != row && j !=col) &&
						(sudoku[i][j].getNumber() == value) && 
						(sudoku[i][j].getBoxId() == sudoku[row][col].getBoxId()))
					return false;	
				j++;
			}
			i++;
		}

		return true;
	}
}
