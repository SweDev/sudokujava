package Sudoku_model;
/**
 * This Square class represents a square on a 
 * Sudoku game board. 
 * 
 * The Square class creates squares and provides
 * was to change or access properties of a square.
 * A square object is a building block of a Sudoku
 * board.
 *  
 * This class does not proved any GUI. This class is part 
 * of a model behind the GUI.
 * 
 * @author      Patrik Paradis
 * @version		LTH University, ready for assessment version.
 */
public class Square {

	// Indicates if a square has a number or not.
	private boolean hasNumber;
	// Gives the number that a square has.
	private int number;
	// Each square is mapped to a specific box by 
	// an integer number on the game board.
	// Each board has six squares mapped to it.
	private int boxId;
	
	/**
	 * Constructor for creating square objects and 
	 * initialize the properties of a square.
	 */
	public Square() {
		hasNumber = false;
		number = -1;
		boxId = -1;
	}
	
	/**
	 * Exposes a way to get an integer value that a square has.
	 * <p>
	 * 
	 * @return an integer number between 1...9.
	 */
	public int getNumber(){
		return number;
	}
	
	/**
	 * Exposes a way to set an integer value on a square.
	 * Must be an integer value 1...9.
	 * <p>
	 * 
	 * @param number the number to be assigned onto a square.
	 */
	public void setNumber(int number){
		this.number = number;
	}
	
	/**
	 * Exposes a way to get an id number that is used to 
	 * map every square on the board to one of nine 
	 * "boxes" on the board.
	 * <p>
	 * 
	 * @return an integer number between 1...9.
	 */
	public int getBoxId(){
		return boxId;
	}
	
	/**
	 * Exposes a way to set an id number that is used to 
	 * map every square on the board to one of nine 
	 * "boxes" on the board.
	 * <p>
	 * 
	 * @param boxId an integer number between 1...9.
	 */
	public void setBoxId(int boxId){
		this.boxId = boxId;
	}
	
	/**
	 * Exposes a way to find out if a square on the
	 * board has a number or not.
	 * <p>
	 * 
	 * @return <code>true</code> if the square has a number.
	 * 		   <code>false</code> otherwise.
	 */
	public boolean getHasNumber(){
		return hasNumber;
	}
	
	/**
	 * Exposes a way to set a boolean value (true/false) 
	 * property for a square.
	 * <p>
	 * 
	 * @param hasNumber a boolean value.
	 */
	public void setHasNumber(boolean hasNumber){
		this.hasNumber = hasNumber;
	}
}
